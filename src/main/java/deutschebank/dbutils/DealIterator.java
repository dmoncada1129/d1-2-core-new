package deutschebank.dbutils;

import java.sql.Date;
import java.sql.ResultSet;
import java.sql.SQLException;

public class DealIterator {
    ResultSet rowIterator;

    DealIterator( ResultSet rs )
    {
        rowIterator = rs;
    }

    public boolean  first() throws SQLException
    {
        return rowIterator.first();
    }

    public boolean last() throws SQLException
    {
        return rowIterator.last();
    }
    public boolean next() throws SQLException
    {
        return rowIterator.next();
    }

    public boolean prior() throws SQLException
    {
        return rowIterator.previous();
    }

    public  int  getDealID() throws SQLException
    {
        return rowIterator.getInt("deal_id");
    }

    public String getDealTime() throws SQLException
    {
        return rowIterator.getString("deal_time");
    }

    public String getDealType() throws SQLException
    {
        return rowIterator.getString("deal_type");
    }

    public  double  getDealAmount() throws SQLException
    {
        return rowIterator.getInt("deal_amount");
    }

    public  int  getDealQuantity() throws SQLException
    {
        return rowIterator.getInt("deal_quantity");
    }
//
//    public   String  getInstrumentName() throws SQLException
//    {
//        return rowIterator.getString("instrument_name");
//    }
//
//    public String getCounterpartyName() throws SQLException {
//        return rowIterator.getString("counterparty_name");
//    }
//
//    public String getCounterpartyStatus() throws SQLException {
//        return rowIterator.getString("counterparty_status");
//    }
//
//    public String getCounterpartyDate() throws SQLException{
//        return rowIterator.getString("counterparty_date_registered");
//    }

    Deal   buildDeal() throws SQLException
    {
//        Deal result = new Deal( getDealID(), getDealTime(), getDealType(), getDealAmount(), getDealQuantity(), getInstrumentName(),getCounterpartyName(),getCounterpartyStatus(), getCounterpartyDate() );
        Deal result = new Deal( getDealID(), getDealTime(), getDealType(), getDealAmount(), getDealQuantity());
        return result;
    }
}
