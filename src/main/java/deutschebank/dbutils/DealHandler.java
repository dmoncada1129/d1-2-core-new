package deutschebank.dbutils;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import deutschebank.MainUnit;

import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

public class DealHandler {
    static  private DealHandler itsSelf = null;

    private DealHandler(){}

    static  public  DealHandler  getLoader()
    {
        if( itsSelf == null )
            itsSelf = new DealHandler();
        return itsSelf;
    }

////    public  Deal  loadFromDB(Connection theConnection, String userid, String pwd )
//        public  Deal  loadFromDB(Connection theConnection, String userid, String pwd ) {
//        Deal result = null;
//        try
//        {
//            MainUnit.log("On Entry -> DealHandler.loadFromDB()");
////            String sbQuery = "select * from db_grad.deal where user_id=? and user_pwd=?";
//            String sbQuery = "select * from db_grad.deal";
//            PreparedStatement stmt = theConnection.prepareStatement(sbQuery);
////            stmt.setString(1, userid);
////            stmt.setString(2, pwd);
//            ResultSet rs = stmt.executeQuery();
//
//            DealIterator iter = new DealIterator(rs);
//
//            if( iter.next() )
//            {
//                result = iter.buildDeal();
//                MainUnit.log(result.getDealID() + "//" + result.getDealTime() );
//            }
//        }
//        catch (SQLException ex)
//        {
//            Logger.getLogger(UserHandler.class.getName()).log(Level.SEVERE, null, ex);
//        }
//
//        MainUnit.log("On Exit -> UserHandler.loadFromDB()");
//
//        return result;
//    }

    public  ArrayList<Deal>  loadFromDB( Connection theConnection )
    {
        ArrayList<Deal> result = new ArrayList<Deal>();
        Deal theDeal = null;
        try
        {
            String sbQuery = "select * from db_grad_cs_1917.deal";
            PreparedStatement stmt = theConnection.prepareStatement(sbQuery);
            ResultSet rs = stmt.executeQuery();

            DealIterator iter = new DealIterator(rs);

            while( iter.next() )
            {
                theDeal = iter.buildDeal();
                if(MainUnit.debugFlag)
                    System.out.println( theDeal.getDealID() + "//" + theDeal.getDealTime() );
                result.add(theDeal);
            }
        }
        catch (SQLException ex)
        {
            Logger.getLogger(InstrumentHandler.class.getName()).log(Level.SEVERE, null, ex);
        }

        return result;
    }

//    public  String  toJSON( Deal theDeal )
//    {
//        String result = "<Some value from the server>";
//        try
//        {
//            ObjectMapper mapper = new ObjectMapper();
//            result = mapper.writeValueAsString(theDeal);
//        }
//        catch (JsonProcessingException ex)
//        {
//            Logger.getLogger(UserHandler.class.getName()).log(Level.SEVERE, null, ex);
//        } catch (IOException ex)
//        {
//            Logger.getLogger(UserHandler.class.getName()).log(Level.SEVERE, null, ex);
//        }
//        return result;
//    }

    public  String  toJSON( ArrayList<Deal> theDeals )
    {
        String result = "";
        Deal[] dealArray = new Deal[theDeals.size()];
        theDeals.toArray(dealArray);
        try
        {
            ObjectMapper mapper = new ObjectMapper();
            result = mapper.writeValueAsString(dealArray);
        }
        catch (JsonProcessingException ex)
        {
            Logger.getLogger(DealHandler.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex)
        {
            Logger.getLogger(InstrumentHandler.class.getName()).log(Level.SEVERE, null, ex);
        }
        return result;
    }
}

